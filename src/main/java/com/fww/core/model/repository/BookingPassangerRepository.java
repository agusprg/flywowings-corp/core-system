package com.fww.core.model.repository;

import com.fww.core.model.entity.BookingPassanger;
import com.fww.core.model.entity.BookingTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingPassangerRepository extends JpaRepository<BookingPassanger, Long> {
}
