package com.fww.core.model.repository;

import com.fww.core.model.entity.FlightSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Date;
import java.util.Optional;


public interface FlightScheduleRepository extends JpaRepository<FlightSchedule, Long> {

    @Query("SELECT FS FROM FlightSchedule FS JOIN FS.flight F WHERE F.origin = :origin " +
            " AND F.destination =:destination AND schedule =:date")
    List<FlightSchedule> findByOriginAndDestinationAndSchedule(Long origin, Long destination, Date date);
    @Query("SELECT FS FROM FlightSchedule FS JOIN FS.flight F WHERE F.origin = :origin " +
            " AND F.destination =:destination AND schedule =:date AND F.code = :flightCode ")
    Optional<FlightSchedule> findDetailFlight(
            Long origin, Long destination, Date date, String flightCode);
}