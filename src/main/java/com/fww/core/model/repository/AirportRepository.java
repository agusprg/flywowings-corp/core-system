package com.fww.core.model.repository;

import com.fww.core.model.entity.Airport;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AirportRepository extends JpaRepository<Airport, Long> {
}
