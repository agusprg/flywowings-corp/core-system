package com.fww.core.model.repository;

import com.fww.core.dto.response.BaseResponse;
import com.fww.core.model.entity.Airport;
import com.fww.core.model.entity.BookingTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BookingTransactionRepository extends JpaRepository<BookingTransaction, Long> {
    Optional<BookingTransaction> findByTransactionId(String transactionId);
}
