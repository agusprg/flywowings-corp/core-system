package com.fww.core.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightSchedule {

    @Id
    Long id;

    @OneToOne()
    @JoinColumn(name="flightId",referencedColumnName="id", insertable = false, updatable = false)
    private Flight flight;

    Long flightId;

    @Temporal(TemporalType.DATE)
    Date schedule;

    Integer availableSeat;
    Integer reservedSeat;

    @Transient
    Integer totalSeat;
    @Transient
    Double price;
    @Transient
    Long duration;
    @Transient
    Integer capacity;

    public Integer getTotalSeat() {
        return flight.getPlane().getSeatCapacity();
    }
    public Long getDuration() { return flight.getDuration(); }
    public Integer getCapacity() {return flight.getPlane().getSeatCapacity();}

    public Double getPrice(){
        if((availableSeat / getTotalSeat()) < 20)
            return flight.getHighPrice();
        else if ((availableSeat / getTotalSeat()) < 20) {
            return flight.getLowPrice();
        }else {
            return flight.getNormalPrice();
        }
    }
}