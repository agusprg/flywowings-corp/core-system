package com.fww.core.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Data
@Entity
public class Seat {

    @Id
    Long id;

    @OneToOne()
    @JoinColumn(name="flightScheduleId",referencedColumnName="id", insertable = false, updatable = false)
    private FlightSchedule flight;

    Long flightScheduleId;

    @OneToOne()
    @JoinColumn(name="passangerId",referencedColumnName="id", insertable = false, updatable = false)
    private BookingPassanger passanger;
    Long passangerId;
}
