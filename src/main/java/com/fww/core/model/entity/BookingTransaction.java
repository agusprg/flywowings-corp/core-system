package com.fww.core.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fww.core.dto.enums.TransactionStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookingTransaction implements Serializable {

    @Id
    @GeneratedValue(generator = "bookingTransactionSeq")
    @SequenceGenerator(name = "bookingTransactionSeq", sequenceName = "SEQ_BOOKING_TRANSACTION", allocationSize = 1)
    @JsonIgnore
    private Long id;

    private String transactionId;
    private String bookingCode;
    private String channel;
    private String email;
    private String name;
    private String contact;
    private String flightCode;

    @Temporal(TemporalType.DATE)
    private Date flightDate;
    private Time departure;
    private Time arrival;
    private Double price;
    private Integer totalPassanger;
    private Double totalPrice;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date transactionDate;

    @Enumerated(EnumType.STRING)
    private TransactionStatus transactionStatus;
    private String transactionStatusDescription;

    @OneToMany()
    @JoinColumn(name="transactionId",referencedColumnName="id", insertable = false, updatable = false)
    List<BookingPassanger> passanger;

    @Transient
    String callback;

    @Transient
    String channelCallback;

}