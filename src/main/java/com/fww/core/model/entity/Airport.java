package com.fww.core.model.entity;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
public class Airport {

    @Id
    Long id;

    String name;
    String code;
    String province;
    String city;
    String country;
}