package com.fww.core.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Plane {

    @Id
    Long id;

    String code;
    Integer seatCapacity;
}