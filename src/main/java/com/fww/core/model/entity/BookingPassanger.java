package com.fww.core.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.util.Date;

@Data
@Entity
public class BookingPassanger {
    @Id
    @GeneratedValue(generator = "bookingPassangerSeq")
    @SequenceGenerator(name = "bookingPassangerSeq", sequenceName = "SEQ_BOOKING_PASSANGER", allocationSize = 1)
    private Long id;

    private Long transactionId;
    private String firstName;
    private String lastName;
    private String identityNumber;
    private Date DOB;
}
