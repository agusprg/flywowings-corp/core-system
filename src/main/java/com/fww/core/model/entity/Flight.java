package com.fww.core.model.entity;


import lombok.Data;

import javax.persistence.*;
import java.sql.Time;

@Data
@Entity
public class Flight {

    @Id
    Long id;

    @OneToOne()
    @JoinColumn(name="planeId",referencedColumnName="id", insertable = false, updatable = false)
    Plane plane;

    String code;
    Long planeId;
    Long origin;
    Long destination;
    Time timeDeparture;
    Time timeArrival;
    Long duration;
    Double normalPrice;
    Double lowPrice;
    Double highPrice;
}
