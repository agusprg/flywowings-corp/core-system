package com.fww.core.service;

import com.fww.core.dto.enums.TransactionStatus;
import com.fww.core.dto.request.BookingRequest;
import com.fww.core.dto.request.CallbackRequest;
import com.fww.core.dto.response.BaseResponse;
import com.fww.core.dto.response.FlightScheduleList;
import com.fww.core.dto.response.FlightScheduleResponse;
import com.fww.core.helper.BusinessException;
import com.fww.core.model.entity.Airport;
import com.fww.core.model.entity.BookingPassanger;
import com.fww.core.model.entity.BookingTransaction;
import com.fww.core.model.entity.FlightSchedule;
import com.fww.core.model.repository.AirportRepository;
import com.fww.core.model.repository.BookingPassangerRepository;
import com.fww.core.model.repository.BookingTransactionRepository;
import com.fww.core.model.repository.FlightScheduleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.fww.core.dto.enums.TransactionStatus.*;
import static com.fww.core.helper.BookingCodeHelper.generateRandomCode;
import static com.fww.core.helper.BookingCodeHelper.generateTrxId;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FlightService {
    private final FlightScheduleRepository flightScheduleRepository;
    private final AirportRepository airportRepository;
    private final BookingTransactionRepository bookingTransactionRepository;
    private final BookingPassangerRepository bookingPassangerRepository;
    private final BpmService bpmService;
    ModelMapper modelMapper = new ModelMapper();

    @Value("${endpoint.callback}")
    private String callback;

    private Map<String, Long> getMappedAirportCity() {
        Map<String, Long> mappedCity = new HashMap<>();
        List<Airport> airports = airportRepository.findAll();
        airports.stream().forEach(airport -> mappedCity.put(airport.getCode(),airport.getId()));
        return mappedCity;
    }

    public BaseResponse<FlightScheduleResponse> searchFlight(String origin, String destination, Date date) {

        Map<String,Long> mappedCity = getMappedAirportCity();
        List<FlightSchedule> flightSchedules = flightScheduleRepository.
                findByOriginAndDestinationAndSchedule(mappedCity.get(origin.toUpperCase()),
                        mappedCity.get(destination.toUpperCase()),date);
        List<FlightScheduleList> flightScheduleDto = flightSchedules.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
        FlightScheduleResponse flightScheduleResponse = FlightScheduleResponse.builder()
                .date(date)
                .origin(origin)
                .destination(destination)
                .schedule(flightScheduleDto)
                .build();
        return BaseResponse.buildSuccessResponse(flightScheduleResponse);
    }

    private FlightScheduleList convertToDto(FlightSchedule flightSchedule) {
        return modelMapper.map(flightSchedule, FlightScheduleList.class);
    }

    @Transactional
    public BaseResponse<Object> bookingFlight(BookingRequest request) {
        Map<String,Long> mappedCity = getMappedAirportCity();
        FlightSchedule fs = flightScheduleRepository.
                findDetailFlight(
                        mappedCity.get(request.getOrigin().toUpperCase()),
                        mappedCity.get(request.getDestination().toUpperCase()),
                        request.getFlightDate(),
                        request.getFlightCode()).orElseThrow(BusinessException::flightNotFound);

        if(fs.getAvailableSeat() < request.getPassangerTotal())
            throw BusinessException.flightNotFound();

        BookingTransaction bookingTransaction = BookingTransaction.builder()
                .transactionId(generateTrxId())
                .bookingCode(generateRandomCode())
                .channel(request.getChannel())
                .email(request.getEmail())
                .name(request.getName())
                .contact(request.getContact())
                .flightCode(fs.getFlight().getCode())
                .flightDate(request.getFlightDate())
                .departure(fs.getFlight().getTimeDeparture())
                .arrival(fs.getFlight().getTimeArrival())
                .price(fs.getPrice())
                .totalPassanger(request.getPassangerTotal())
                .totalPrice(fs.getPrice() * request.getPassangerTotal())
                .transactionDate(new Date())
                .transactionStatus(TransactionStatus.INIT)
                .build();
        bookingTransactionRepository.save(bookingTransaction);

        List<BookingPassanger> bps = request.getPassangerList().stream().map(passanger -> {
            BookingPassanger bp = modelMapper.map(passanger, BookingPassanger.class);
            bp.setTransactionId(bookingTransaction.getId());
            return bp;
        }).collect(Collectors.toList());
        bookingPassangerRepository.saveAll(bps);

        bookingTransaction.setPassanger(bps);
        bookingTransaction.setChannelCallback(request.getCallback());
        bookingTransaction.setCallback(callback);
        bpmService.sendToProcessBooking(bookingTransaction);

        return BaseResponse.buildSuccessResponse(bookingTransaction);
    }

    public BaseResponse<BookingTransaction> searchFlightHistory(String transactionId) {
        return BaseResponse.createSuccessResponse(
                bookingTransactionRepository.findByTransactionId(transactionId)
                        .orElseThrow(BusinessException::flightNotFound)
        );
    }

    public BaseResponse<Object> callback(CallbackRequest request) {
        log.info("payload {}",request);

        if(request.getTransactionStatus().equals(FAILED)){
            BookingTransaction transaction = bookingTransactionRepository.findByTransactionId(request.getTransactionId())
                    .orElseThrow(BusinessException::flightNotFound);
            transaction.setTransactionStatus(FAILED);
            transaction.setTransactionStatusDescription(request.getTransactionStatusDescription());
            bookingTransactionRepository.save(transaction);
        }else if(request.getTransactionStatus().equals(EXPIRED)){
            BookingTransaction transaction = bookingTransactionRepository.findByTransactionId(request.getTransactionId())
                    .orElseThrow(BusinessException::flightNotFound);
            if(transaction.getTransactionStatus().equals(WAITING_PAYMENT)){
                transaction.setTransactionStatus(EXPIRED);
                transaction.setTransactionStatusDescription(request.getTransactionStatusDescription());
                bookingTransactionRepository.save(transaction);
            }
        }

        //to do notify callback to channel

        return BaseResponse.createSuccessResponse();
    }
}