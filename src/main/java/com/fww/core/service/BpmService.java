package com.fww.core.service;

import com.fww.core.model.entity.BookingTransaction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BpmService {

    private final RestTemplate restTemplate;

    @Value("${endpoint.bpmn}")
    private String endpoint;

    public void sendToProcessBooking(BookingTransaction request){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));

        HttpEntity<BookingTransaction> entity = new HttpEntity<>(request, headers);
        restTemplate.exchange(endpoint, HttpMethod.POST, entity, Object.class).getBody();
    }
}
