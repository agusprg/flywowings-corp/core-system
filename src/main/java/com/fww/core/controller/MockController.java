package com.fww.core.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fww.core.dto.Passanger;
import com.fww.core.dto.response.BaseResponse;
import com.fww.core.helper.BusinessException;
import com.fww.core.model.entity.BookingPassanger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * mock service to invoke checking peduli lindungi & dukcapil
 * **/

@RestController
public class MockController {
    @PostMapping("mock/dukcapil")
    BaseResponse dukcapil(@RequestBody String psg) throws JsonProcessingException {
        AtomicReference<Boolean> isGreen = new AtomicReference<>(true);
        ObjectMapper mapper = new ObjectMapper();
        List<Passanger> lsp =mapper.readValue(psg , new TypeReference<List<Passanger>>() {});

        lsp.forEach(ps -> {
            if(ps.getFirstName().contains("dkxx"))
                isGreen.set(false);
        });

        return isGreen.get() ?
                BaseResponse.createSuccessResponse():
                BaseResponse.createFailedResponse(BusinessException.dukcapilError());
    }

    @PostMapping("mock/pl")
    BaseResponse pl(@RequestBody String psg) throws JsonProcessingException {
        AtomicReference<Boolean> isGreen = new AtomicReference<>(true);
        ObjectMapper mapper = new ObjectMapper();
        List<Passanger> lsp =mapper.readValue(psg , new TypeReference<List<Passanger>>() {});

        lsp.forEach(ps -> {
            if(ps.getFirstName().contains("plxx"))
                isGreen.set(false);
        });

        return isGreen.get() ?
                BaseResponse.createSuccessResponse():
                BaseResponse.createFailedResponse(BusinessException.plError());
    }

}
