package com.fww.core.controller;

import com.fww.core.dto.request.BookingRequest;
import com.fww.core.dto.request.CallbackRequest;
import com.fww.core.dto.response.BaseResponse;
import com.fww.core.dto.response.FlightScheduleResponse;
import com.fww.core.model.entity.BookingTransaction;
import com.fww.core.service.FlightService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("flight")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Flight {

    private final FlightService flightService;

    @GetMapping("search")
    BaseResponse<FlightScheduleResponse> searchFlight(
            @RequestParam("origin") String origin,
            @RequestParam("destination") String destination,
            @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date){

        return flightService.searchFlight(origin,destination,date);
    }

    @PostMapping("booking")
    BaseResponse<Object> bookingFlight(@Valid @RequestBody BookingRequest request){
        return flightService.bookingFlight(request);
    }

    @GetMapping("history")
    BaseResponse<BookingTransaction> searchFlightHistory(@RequestParam("transaction_id") String transactionId){
        return flightService.searchFlightHistory(transactionId);
    }

    @PostMapping("callback")
    BaseResponse<Object> bookingFlight(@Valid @RequestBody CallbackRequest request){
        return flightService.callback(request);
    }
}