package com.fww.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightScheduleList {
    private String flightCode;
    @JsonProperty("time_departure")
    private Date flightTimeDeparture;
    @JsonProperty("time_arrival")
    private Date flightTimeArrival;
    private Integer duration;
    private Integer availableSeat;
    private Integer capacity;
    private Long price;
}
