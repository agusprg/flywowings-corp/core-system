package com.fww.core.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FlightScheduleResponse {
    private String origin;
    private String destination;
    private Date date;
    private List<FlightScheduleList> schedule;

}
