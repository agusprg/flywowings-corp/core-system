package com.fww.core.dto.response;

import com.fww.core.dto.enums.TransactionStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BookingResponse {
    private String TransactionId;
    private TransactionStatus status;
    private String statusDescription;

}
