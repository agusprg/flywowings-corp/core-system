package com.fww.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageInfo {
    private String code;
    private String title;
    private String message;
    private List errorList;
}
