package com.fww.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fww.core.helper.BusinessException;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BaseResponse<T> {
    private Integer status;
    private String message;
    private T data;
    private MessageInfo messageInfo;
    private Object exception;
    private static final String WARNING = "ATTENTION";

    public static BaseResponse createSuccessResponse() {
        BaseResponse response = new BaseResponse();
        response.setStatus(0);
        response.setMessage("ok");
        return response;
    }

    public static BaseResponse createSuccessResponse(Object data) {
        BaseResponse response = createSuccessResponse();
        response.setData(data);
        return response;
    }

    public static <T> BaseResponse<T> buildSuccessResponse(T data) {
        BaseResponse<T> response = new BaseResponse<T>();
        response.setStatus(0);
        response.setMessage("ok");
        response.setData(data);
        return response;
    }

    public static BaseResponse createSuccessResponseWithSinglePayload(String key, Object payloadData) {
        Map<String, Object> data = new HashMap<>();
        data.put(key, payloadData);
        return createSuccessResponse(data);
    }

    public static BaseResponse createSuccessResponseWithDoublePayload(String key, Object payloadData,String key2, Object payloadData2) {
        Map<String, Object> data = new HashMap<>();
        data.put(key, payloadData);
        data.put(key2, payloadData2);
        return createSuccessResponse(data);
    }

    public static BaseResponse createSuccessResponseWithDoublePayload(String key, Object payloadData,String key2, Object payloadData2,String key3, Object payloadData3) {
        Map<String, Object> data = new HashMap<>();
        data.put(key, payloadData);
        data.put(key2, payloadData2);
        data.put(key3, payloadData3);
        return createSuccessResponse(data);
    }

    public static BaseResponse createFailedResponse(Integer statusCode, String message){
        BaseResponse response = new BaseResponse();
        response.setStatus(statusCode);
        response.setMessage(message);
        return response;
    }

    public static <T> BaseResponse<T> createFailedResponse(Integer statusCode, String message, T data) {
        BaseResponse<T> response = new BaseResponse<T>();
        response.setStatus(statusCode);
        response.setMessage(message);
        response.setData(data);
        return response;
    }

    public static BaseResponse createFailedResponse(Integer statusCode, List<String> errorList) {
        BaseResponse response = new BaseResponse();
        response.setStatus(1);
        MessageInfo info = new MessageInfo();
        info.setCode(statusCode.toString());
        info.setTitle(WARNING);
        info.setErrorList(errorList);
        response.setMessageInfo(info);
        return response;
    }


    public static BaseResponse createFailedResponse(BusinessException ex) {
        BaseResponse response = new BaseResponse();
        response.setStatus(1);
        response.setMessage("Business Error");
        MessageInfo info = new MessageInfo();
        info.setCode(ex.getErrorCode());
        info.setMessage(ex.getMessage());
        info.setTitle(WARNING);
        response.setMessageInfo(info);
        return response;
    }

    public static BaseResponse createFailedResponse(Integer statusCode, BusinessException ex) {
        BaseResponse response = new BaseResponse();
        response.setStatus(statusCode);
        response.setMessage(ex.getMessage());
        return response;
    }

    public static BaseResponse createSuccessResponse(Integer statusCode, String message) {
        BaseResponse response = new BaseResponse();
        response.setStatus(statusCode);
        response.setMessage(message);
        return response;
    }

}
