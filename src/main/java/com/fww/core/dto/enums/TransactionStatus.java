package com.fww.core.dto.enums;

public enum TransactionStatus {
    INIT,
    WAITING_PAYMENT,
    FAILED,
    EXPIRED,
    SUCCESS
}
