package com.fww.core.dto.request;

import com.fww.core.dto.Passanger;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@Valid
public class BookingRequest {
    @NotBlank(message = "mandatory field")
    private String flightCode;
    @NotBlank(message = "mandatory field")
    private String channel;
    @NotBlank(message = "mandatory field")
    private String origin;
    @NotBlank(message = "mandatory field")
    private String destination;
    @NotNull(message = "mandatory field")
    private Date flightDate;
    @NotBlank(message = "mandatory field")
    private String email;
    @NotBlank(message = "mandatory field")
    private String contact;
    @NotBlank(message = "mandatory field")
    private String name;
    @NotNull(message = "mandatory field")
    private Long price;
    @NotNull(message = "mandatory field")
    private Long totalPrice;
    @NotNull(message = "mandatory field")
    private Integer passangerTotal;
    @NotBlank(message = "mandatory field")
    private String callback;
    @NotNull(message = "mandatory field")
    private List<Passanger> passangerList;
}
