package com.fww.core.dto.request;

import com.fww.core.dto.enums.TransactionStatus;
import lombok.Data;


@Data
public class CallbackRequest {
    private String transactionId;
    private TransactionStatus transactionStatus;
    private String transactionStatusDescription;
    private String channelCallback;
}

