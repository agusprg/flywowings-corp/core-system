package com.fww.core.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class Passanger implements Serializable {
    private String firstName;
    private String lastName;
    private String identityNumber;
    private Date DOB;
}
