package com.fww.core.helper;

import com.fww.core.dto.response.BaseResponse;
import com.fww.core.helper.BaseConstants;
import com.fww.core.helper.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;

import static com.fww.core.helper.StringConverter.camelToSnake;

@RestControllerAdvice
@Slf4j
public class ControllerErrorExceptionHandler {

    @ExceptionHandler(BusinessException.class)
    protected ResponseEntity<BaseResponse> handleConflict(BusinessException exception, WebRequest request) {
        log.error("[EXCEPTION]: ", exception);
        log.error("[REQUEST]: "+ request.getDescription(true).toString());

        BaseResponse commonResponse = BaseResponse.createFailedResponse(exception);
        return new ResponseEntity<>(commonResponse, exception.getHttpStatus());
    }

    @ExceptionHandler({
            MissingServletRequestParameterException.class,
            MethodArgumentTypeMismatchException.class,
            ConstraintViolationException.class,
            HttpMessageNotReadableException.class
    })
    public ResponseEntity<BaseResponse> handleMissingParams(Exception exception,WebRequest request) {
        log.error("[EXCEPTION]: ", exception);
        log.error("[REQUEST]: "+ request.getDescription(true).toString());

        BaseResponse commonResponse = BaseResponse.createFailedResponse(HttpStatus.BAD_REQUEST.value(),exception.getMessage());

        commonResponse.setMessage("Input Parameter Error "+
                ((exception.getClass() == HttpMessageNotReadableException.class)? BaseConstants.MISSING_JSON_BODY:null)
        );
        return new ResponseEntity<>(commonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<BaseResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception, WebRequest request) {
        BindingResult result = exception.getBindingResult();

        List<String> errorList = new ArrayList<>();
        result.getFieldErrors().forEach((fieldError) -> {
            errorList.add(camelToSnake(fieldError.getField())+" : " +fieldError.getDefaultMessage() +" -> rejected value [" +fieldError.getRejectedValue() +"]" );
        });
        result.getGlobalErrors().forEach((fieldError) -> {
            errorList.add(fieldError.getObjectName()+" : " +fieldError.getDefaultMessage() );
        });

        BaseResponse commonResponse = BaseResponse.createFailedResponse(HttpStatus.BAD_REQUEST.value(),errorList);
        commonResponse.setMessage(BaseConstants.VALIDATION_ERROR);
        return new ResponseEntity<>(commonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({RuntimeException.class,NullPointerException.class})
    public ResponseEntity<BaseResponse> runTimeException(RuntimeException exception,WebRequest request) {
        log.error("[EXCEPTION]: ", exception);
        log.error("[REQUEST]: "+ request.getDescription(true).toString());

        BaseResponse commonResponse = BaseResponse.createFailedResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),exception.getMessage());
        commonResponse.setMessage("Server Error");
        return new ResponseEntity<>(commonResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
