package com.fww.core.helper;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.stream.Collectors;

public class BookingCodeHelper {
    private static SecureRandom secureRandom = new SecureRandom();
    public static String generateTrxId() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmssSSS");
        return String.format("%s%s%06d",  "FWTX", sdf.format(date), secureRandom.nextInt(1000000));
    }

    public static String generateRandomCode(){
        int length = 8;
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789";
        return new Random().ints(length, 0, chars.length())
                .mapToObj(i -> "" + chars.charAt(i))
                .collect(Collectors.joining());
    }
}