package com.fww.core;

import com.fww.core.dto.response.FlightScheduleList;
import com.fww.core.dto.response.FlightScheduleResponse;
import com.fww.core.model.entity.FlightSchedule;
import com.fww.core.model.repository.FlightScheduleRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class t {

    private final FlightScheduleRepository repository;

    @GetMapping("ok")
    Object ok(){
        Map<String,Object> m = new HashMap<>();

        FlightSchedule a = repository.findAll().get(0);
        List<FlightSchedule> flights = repository.findAll();
        List<FlightScheduleList> list = flights.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());

        return FlightScheduleResponse.builder()
                .date(new Date())
                .origin("SUB")
                .destination("CGK")
                .schedule(list)
                .build();
    }

    private FlightScheduleList convertToDto(FlightSchedule flightSchedule) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(flightSchedule, FlightScheduleList.class);
    }
}
